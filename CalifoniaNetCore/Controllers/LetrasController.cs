﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;

namespace CalifoniaNetCore.Controllers
{
    public class LetrasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LetrasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Letras
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Letra.Include(l => l.EstrofeLetra).Include(l => l.RefraoLetra);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Letras/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letra = await _context.Letra
                .Include(l => l.EstrofeLetra)
                .Include(l => l.RefraoLetra)
                .FirstOrDefaultAsync(m => m.LetraID == id);
            if (letra == null)
            {
                return NotFound();
            }

            return View(letra);
        }

        // GET: Letras/Create
        public IActionResult Create()
        {
            ViewData["EstrofeLetraID"] = new SelectList(_context.EstrofeLetra, "EstrofeLetraID", "EstrofeLetraID");
            ViewData["RefraoLetraID"] = new SelectList(_context.RefraoLetra, "RefraoLetraID", "RefraoLetraID");
            return View();
        }

        // POST: Letras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LetraID,RefraoLetraID,EstrofeLetraID")] Letra letra)
        {
            if (ModelState.IsValid)
            {
                _context.Add(letra);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EstrofeLetraID"] = new SelectList(_context.EstrofeLetra, "EstrofeLetraID", "EstrofeLetraID", letra.EstrofeLetraID);
            ViewData["RefraoLetraID"] = new SelectList(_context.RefraoLetra, "RefraoLetraID", "RefraoLetraID", letra.RefraoLetraID);
            return View(letra);
        }

        // GET: Letras/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letra = await _context.Letra.FindAsync(id);
            if (letra == null)
            {
                return NotFound();
            }
            ViewData["EstrofeLetraID"] = new SelectList(_context.EstrofeLetra, "EstrofeLetraID", "EstrofeLetraID", letra.EstrofeLetraID);
            ViewData["RefraoLetraID"] = new SelectList(_context.RefraoLetra, "RefraoLetraID", "RefraoLetraID", letra.RefraoLetraID);
            return View(letra);
        }

        // POST: Letras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LetraID,RefraoLetraID,EstrofeLetraID")] Letra letra)
        {
            if (id != letra.LetraID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(letra);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LetraExists(letra.LetraID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EstrofeLetraID"] = new SelectList(_context.EstrofeLetra, "EstrofeLetraID", "EstrofeLetraID", letra.EstrofeLetraID);
            ViewData["RefraoLetraID"] = new SelectList(_context.RefraoLetra, "RefraoLetraID", "RefraoLetraID", letra.RefraoLetraID);
            return View(letra);
        }

        // GET: Letras/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letra = await _context.Letra
                .Include(l => l.EstrofeLetra)
                .Include(l => l.RefraoLetra)
                .FirstOrDefaultAsync(m => m.LetraID == id);
            if (letra == null)
            {
                return NotFound();
            }

            return View(letra);
        }

        // POST: Letras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var letra = await _context.Letra.FindAsync(id);
            _context.Letra.Remove(letra);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LetraExists(int id)
        {
            return _context.Letra.Any(e => e.LetraID == id);
        }
    }
}
