﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;
using CalifoniaNetCore.ViewModel;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Text;

namespace CalifoniaNetCore.Controllers
{
    public class PublicacoesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _env;

        public PublicacoesController(ApplicationDbContext context, IHostingEnvironment env)
        {
            _context = context;
            _env = env;
        }

        // GET: Publicacoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Publicacao.ToListAsync());
        }

        // GET: Publicacoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publicacao = await _context.Publicacao
                .FirstOrDefaultAsync(m => m.PublicacaoID == id);
            if (publicacao == null)
            {
                return NotFound();
            }

            return View(publicacao);
        }

        // GET: Publicacoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Publicacoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PublicacaoID,Nome,DataPublicacao,Edicao")] Publicacao publicacao)
        {
            if (ModelState.IsValid)
            {
                _context.Add(publicacao);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(publicacao);
        }

        // GET: Publicacoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publicacao = await _context.Publicacao.FindAsync(id);
            if (publicacao == null)
            {
                return NotFound();
            }
            return View(publicacao);
        }

        // POST: Publicacoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PublicacaoID,Nome,DataPublicacao,Edicao")] Publicacao publicacao)
        {
            if (id != publicacao.PublicacaoID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(publicacao);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PublicacaoExists(publicacao.PublicacaoID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(publicacao);
        }

        // GET: Publicacoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publicacao = await _context.Publicacao
                .FirstOrDefaultAsync(m => m.PublicacaoID == id);
            if (publicacao == null)
            {
                return NotFound();
            }

            return View(publicacao);
        }

        // POST: Publicacoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var publicacao = await _context.Publicacao.FindAsync(id);
            _context.Publicacao.Remove(publicacao);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Publicacoes/Upload
        public IActionResult Upload()
        {
            return View();
        }

        // POST: Publicacoes/Upload
        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile ficheiro)
        {
            List<Publicacao> publicacoesFicheiro = new List<Publicacao>();

            try
            {
                if (ficheiro.Length > 0)
                {
                    //string extensao = Path.GetExtension(model.File.Filename);

                    string path = Path.Combine(_env.WebRootPath, "Uploaded\\Lists", ficheiro.FileName);
                    using (var fs = new FileStream(path, FileMode.Create))
                    {
                        await ficheiro.CopyToAsync(fs);
                    }

                    string caminhoFicheiro = $"Uploaded/Lists/{ficheiro.FileName}";
                    string extension = Path.GetExtension(ficheiro.FileName.Substring(1));

                    if (extension == ".csv")
                    {
                        //Lê o conteúdo do CSV
                        string csvData = System.IO.File.ReadAllText(path, Encoding.UTF8);

                        //Executa um ciclo sobre todas as linhas do ficheiro
                        foreach (string linha in csvData.Split('\n'))
                        {
                            if (!string.IsNullOrEmpty(linha))
                            {
                                publicacoesFicheiro.Add(new Publicacao
                                {
                                    Nome = linha.Split(';')[0].ToString(),
                                    DataPublicacao = DateTime.TryParse(linha.Split(';')[1], out DateTime data) == false ? DateTime.MinValue : data,
                                    Edicao = linha.Split(';')[2].Trim().ToString()
                                });
                            }
                        }

                        if (publicacoesFicheiro.Count > 0)
                        {
                            int contagem = 0;
                            foreach (var item in publicacoesFicheiro)
                            {
                                Publicacao pubAdicionaDb;

                                if (!_context.Publicacao.Any(x => x.Nome == item.Nome))
                                {
                                    //var pubEncontrada = _context.Publicacao.Where(x => x.Nome == item.Nome);
                                    _context.Publicacao.Add(item);
                                    await _context.SaveChangesAsync();
                                    contagem++;
                                }
                            }

                            if (contagem > 0)
                            {
                                ViewData["Success"] = "Ficheiro carregado com sucesso.";
                                return RedirectToAction(nameof(Upload));
                                //return Ok("Ficheiro carregado");
                            }

                            return BadRequest(contagem);
                        }
                        return BadRequest("Ficheiro vazio");
                    }
                    return BadRequest(extension);
                }
                return BadRequest(ficheiro.Length);
            }
            catch (Exception e)
            {

                return BadRequest(e.ToString());
            }
        }

        private bool PublicacaoExists(int id)
        {
            return _context.Publicacao.Any(e => e.PublicacaoID == id);
        }
    }
}
