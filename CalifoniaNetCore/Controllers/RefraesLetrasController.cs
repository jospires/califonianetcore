﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;

namespace CalifoniaNetCore.Controllers
{
    public class RefraesLetrasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RefraesLetrasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RefraesLetras
        public async Task<IActionResult> Index()
        {
            return View(await _context.RefraoLetra.ToListAsync());
        }

        // GET: RefraesLetras/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var refraoLetra = await _context.RefraoLetra
                .FirstOrDefaultAsync(m => m.RefraoLetraID == id);
            if (refraoLetra == null)
            {
                return NotFound();
            }

            return View(refraoLetra);
        }

        // GET: RefraesLetras/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RefraesLetras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RefraoLetraID,TextoRefrao")] RefraoLetra refraoLetra)
        {
            if (ModelState.IsValid)
            {
                _context.Add(refraoLetra);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(refraoLetra);
        }

        // GET: RefraesLetras/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var refraoLetra = await _context.RefraoLetra.FindAsync(id);
            if (refraoLetra == null)
            {
                return NotFound();
            }
            return View(refraoLetra);
        }

        // POST: RefraesLetras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RefraoLetraID,TextoRefrao")] RefraoLetra refraoLetra)
        {
            if (id != refraoLetra.RefraoLetraID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(refraoLetra);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RefraoLetraExists(refraoLetra.RefraoLetraID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(refraoLetra);
        }

        // GET: RefraesLetras/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var refraoLetra = await _context.RefraoLetra
                .FirstOrDefaultAsync(m => m.RefraoLetraID == id);
            if (refraoLetra == null)
            {
                return NotFound();
            }

            return View(refraoLetra);
        }

        // POST: RefraesLetras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var refraoLetra = await _context.RefraoLetra.FindAsync(id);
            _context.RefraoLetra.Remove(refraoLetra);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RefraoLetraExists(int id)
        {
            return _context.RefraoLetra.Any(e => e.RefraoLetraID == id);
        }
    }
}
