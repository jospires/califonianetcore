﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;

namespace CalifoniaNetCore.Controllers
{
    public class ExtensoesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExtensoesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Extensoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Extensao.ToListAsync());
        }

        // GET: Extensoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var extensao = await _context.Extensao
                .FirstOrDefaultAsync(m => m.ExtensaoID == id);
            if (extensao == null)
            {
                return NotFound();
            }

            return View(extensao);
        }

        // GET: Extensoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Extensoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ExtensaoID,Nome")] Extensao extensao)
        {
            if (ModelState.IsValid)
            {
                _context.Add(extensao);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(extensao);
        }

        // GET: Extensoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var extensao = await _context.Extensao.FindAsync(id);
            if (extensao == null)
            {
                return NotFound();
            }
            return View(extensao);
        }

        // POST: Extensoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ExtensaoID,Nome")] Extensao extensao)
        {
            if (id != extensao.ExtensaoID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(extensao);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExtensaoExists(extensao.ExtensaoID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(extensao);
        }

        // GET: Extensoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var extensao = await _context.Extensao
                .FirstOrDefaultAsync(m => m.ExtensaoID == id);
            if (extensao == null)
            {
                return NotFound();
            }

            return View(extensao);
        }

        // POST: Extensoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var extensao = await _context.Extensao.FindAsync(id);
            _context.Extensao.Remove(extensao);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExtensaoExists(int id)
        {
            return _context.Extensao.Any(e => e.ExtensaoID == id);
        }
    }
}
