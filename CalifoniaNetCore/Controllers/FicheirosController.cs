﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CalifoniaNetCore.Data;
using CalifoniaNetCore.Models;

namespace CalifoniaNetCore.Controllers
{
    public class FicheirosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FicheirosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Ficheiros
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Ficheiro.Include(f => f.Extensao);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Ficheiros/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ficheiro = await _context.Ficheiro
                .Include(f => f.Extensao)
                .FirstOrDefaultAsync(m => m.FicheiroID == id);
            if (ficheiro == null)
            {
                return NotFound();
            }

            return View(ficheiro);
        }

        // GET: Ficheiros/Create
        public IActionResult Create()
        {
            ViewData["ExtensaoID"] = new SelectList(_context.Extensao, "ExtensaoID", "ExtensaoID");
            return View();
        }

        // POST: Ficheiros/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FicheiroID,Nome,Url,ExtensaoID")] Ficheiro ficheiro)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ficheiro);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExtensaoID"] = new SelectList(_context.Extensao, "ExtensaoID", "ExtensaoID", ficheiro.ExtensaoID);
            return View(ficheiro);
        }

        // GET: Ficheiros/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ficheiro = await _context.Ficheiro.FindAsync(id);
            if (ficheiro == null)
            {
                return NotFound();
            }
            ViewData["ExtensaoID"] = new SelectList(_context.Extensao, "ExtensaoID", "ExtensaoID", ficheiro.ExtensaoID);
            return View(ficheiro);
        }

        // POST: Ficheiros/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FicheiroID,Nome,Url,ExtensaoID")] Ficheiro ficheiro)
        {
            if (id != ficheiro.FicheiroID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ficheiro);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FicheiroExists(ficheiro.FicheiroID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExtensaoID"] = new SelectList(_context.Extensao, "ExtensaoID", "ExtensaoID", ficheiro.ExtensaoID);
            return View(ficheiro);
        }

        // GET: Ficheiros/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ficheiro = await _context.Ficheiro
                .Include(f => f.Extensao)
                .FirstOrDefaultAsync(m => m.FicheiroID == id);
            if (ficheiro == null)
            {
                return NotFound();
            }

            return View(ficheiro);
        }

        // POST: Ficheiros/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ficheiro = await _context.Ficheiro.FindAsync(id);
            _context.Ficheiro.Remove(ficheiro);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FicheiroExists(int id)
        {
            return _context.Ficheiro.Any(e => e.FicheiroID == id);
        }
    }
}
