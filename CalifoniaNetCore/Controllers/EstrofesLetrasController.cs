﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;

namespace CalifoniaNetCore.Controllers
{
    public class EstrofesLetrasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EstrofesLetrasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: EstrofesLetras
        public async Task<IActionResult> Index()
        {
            return View(await _context.EstrofeLetra.ToListAsync());
        }

        // GET: EstrofesLetras/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var estrofeLetra = await _context.EstrofeLetra
                .FirstOrDefaultAsync(m => m.EstrofeLetraID == id);
            if (estrofeLetra == null)
            {
                return NotFound();
            }

            return View(estrofeLetra);
        }

        // GET: EstrofesLetras/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EstrofesLetras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EstrofeLetraID,Descricao,TextoEstrofe")] EstrofeLetra estrofeLetra)
        {
            if (ModelState.IsValid)
            {
                _context.Add(estrofeLetra);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(estrofeLetra);
        }

        // GET: EstrofesLetras/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var estrofeLetra = await _context.EstrofeLetra.FindAsync(id);
            if (estrofeLetra == null)
            {
                return NotFound();
            }
            return View(estrofeLetra);
        }

        // POST: EstrofesLetras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EstrofeLetraID,Descricao,TextoEstrofe")] EstrofeLetra estrofeLetra)
        {
            if (id != estrofeLetra.EstrofeLetraID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(estrofeLetra);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EstrofeLetraExists(estrofeLetra.EstrofeLetraID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(estrofeLetra);
        }

        // GET: EstrofesLetras/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var estrofeLetra = await _context.EstrofeLetra
                .FirstOrDefaultAsync(m => m.EstrofeLetraID == id);
            if (estrofeLetra == null)
            {
                return NotFound();
            }

            return View(estrofeLetra);
        }

        // POST: EstrofesLetras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var estrofeLetra = await _context.EstrofeLetra.FindAsync(id);
            _context.EstrofeLetra.Remove(estrofeLetra);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EstrofeLetraExists(int id)
        {
            return _context.EstrofeLetra.Any(e => e.EstrofeLetraID == id);
        }
    }
}
