﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CalifoniaNetCore.Data;
using CalifoniaNetCore.Models;

namespace CalifoniaNetCore.Controllers
{
    public class LetristasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LetristasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Letristas
        public async Task<IActionResult> Index()
        {
            return View(await _context.Letrista.ToListAsync());
        }

        // GET: Letristas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letrista = await _context.Letrista
                .FirstOrDefaultAsync(m => m.LetristaID == id);
            if (letrista == null)
            {
                return NotFound();
            }

            return View(letrista);
        }

        // GET: Letristas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Letristas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LetristaID,Nome")] Letrista letrista)
        {
            if (ModelState.IsValid)
            {
                _context.Add(letrista);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(letrista);
        }

        // GET: Letristas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letrista = await _context.Letrista.FindAsync(id);
            if (letrista == null)
            {
                return NotFound();
            }
            return View(letrista);
        }

        // POST: Letristas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LetristaID,Nome")] Letrista letrista)
        {
            if (id != letrista.LetristaID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(letrista);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LetristaExists(letrista.LetristaID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(letrista);
        }

        // GET: Letristas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var letrista = await _context.Letrista
                .FirstOrDefaultAsync(m => m.LetristaID == id);
            if (letrista == null)
            {
                return NotFound();
            }

            return View(letrista);
        }

        // POST: Letristas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var letrista = await _context.Letrista.FindAsync(id);
            _context.Letrista.Remove(letrista);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LetristaExists(int id)
        {
            return _context.Letrista.Any(e => e.LetristaID == id);
        }
    }
}
