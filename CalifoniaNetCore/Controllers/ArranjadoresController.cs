﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;

namespace CalifoniaNetCore.Controllers
{
    public class ArranjadoresController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ArranjadoresController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Arranjadores
        public async Task<IActionResult> Index()
        {
            return View(await _context.Arranjador.ToListAsync());
        }

        // GET: Arranjadores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var arranjador = await _context.Arranjador
                .FirstOrDefaultAsync(m => m.ArranjadorID == id);
            if (arranjador == null)
            {
                return NotFound();
            }

            return View(arranjador);
        }

        // GET: Arranjadores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Arranjadores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ArranjadorID,Nome")] Arranjador arranjador)
        {
            if (ModelState.IsValid)
            {
                _context.Add(arranjador);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(arranjador);
        }

        // GET: Arranjadores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var arranjador = await _context.Arranjador.FindAsync(id);
            if (arranjador == null)
            {
                return NotFound();
            }
            return View(arranjador);
        }

        // POST: Arranjadores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ArranjadorID,Nome")] Arranjador arranjador)
        {
            if (id != arranjador.ArranjadorID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(arranjador);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArranjadorExists(arranjador.ArranjadorID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(arranjador);
        }

        // GET: Arranjadores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var arranjador = await _context.Arranjador
                .FirstOrDefaultAsync(m => m.ArranjadorID == id);
            if (arranjador == null)
            {
                return NotFound();
            }

            return View(arranjador);
        }

        // POST: Arranjadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var arranjador = await _context.Arranjador.FindAsync(id);
            _context.Arranjador.Remove(arranjador);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ArranjadorExists(int id)
        {
            return _context.Arranjador.Any(e => e.ArranjadorID == id);
        }
    }
}
