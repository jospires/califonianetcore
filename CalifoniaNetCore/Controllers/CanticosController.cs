﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CalifoniaNetCore.Data;
using CalifoniaNetCore.Models;

namespace CalifoniaNetCore.Controllers
{
    public class CanticosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CanticosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Canticos
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Cantico.Include(c => c.Compositor).Include(c => c.Letrista);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Canticos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cantico = await _context.Cantico
                .Include(c => c.Compositor)
                .Include(c => c.Letrista)
                .FirstOrDefaultAsync(m => m.CanticoID == id);
            if (cantico == null)
            {
                return NotFound();
            }

            return View(cantico);
        }

        // GET: Canticos/Create
        public IActionResult Create()
        {
            ViewData["CompositorID"] = new SelectList(_context.Compositor, "CompositorID", "CompositorID");
            ViewData["LetristaID"] = new SelectList(_context.Letrista, "LetristaID", "LetristaID");
            return View();
        }

        // POST: Canticos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CanticoID,Titulo,DataComposicao,DataSubmissao,LetristaID,CompositorID,Notas")] Cantico cantico)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cantico);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompositorID"] = new SelectList(_context.Compositor, "CompositorID", "CompositorID", cantico.CompositorID);
            ViewData["LetristaID"] = new SelectList(_context.Letrista, "LetristaID", "LetristaID", cantico.LetristaID);
            return View(cantico);
        }

        // GET: Canticos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cantico = await _context.Cantico.FindAsync(id);
            if (cantico == null)
            {
                return NotFound();
            }
            ViewData["CompositorID"] = new SelectList(_context.Compositor, "CompositorID", "CompositorID", cantico.CompositorID);
            ViewData["LetristaID"] = new SelectList(_context.Letrista, "LetristaID", "LetristaID", cantico.LetristaID);
            return View(cantico);
        }

        // POST: Canticos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CanticoID,Titulo,DataComposicao,DataSubmissao,LetristaID,CompositorID,Notas")] Cantico cantico)
        {
            if (id != cantico.CanticoID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cantico);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CanticoExists(cantico.CanticoID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompositorID"] = new SelectList(_context.Compositor, "CompositorID", "CompositorID", cantico.CompositorID);
            ViewData["LetristaID"] = new SelectList(_context.Letrista, "LetristaID", "LetristaID", cantico.LetristaID);
            return View(cantico);
        }

        // GET: Canticos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cantico = await _context.Cantico
                .Include(c => c.Compositor)
                .Include(c => c.Letrista)
                .FirstOrDefaultAsync(m => m.CanticoID == id);
            if (cantico == null)
            {
                return NotFound();
            }

            return View(cantico);
        }

        // POST: Canticos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cantico = await _context.Cantico.FindAsync(id);
            _context.Cantico.Remove(cantico);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CanticoExists(int id)
        {
            return _context.Cantico.Any(e => e.CanticoID == id);
        }
    }
}
