﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Data;

namespace CalifoniaNetCore.Controllers
{
    public class CompositoresController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CompositoresController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Compositores
        public async Task<IActionResult> Index()
        {
            return View(await _context.Compositor.ToListAsync());
        }

        // GET: Compositores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var compositor = await _context.Compositor
                .FirstOrDefaultAsync(m => m.CompositorID == id);
            if (compositor == null)
            {
                return NotFound();
            }

            return View(compositor);
        }

        // GET: Compositores/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Compositores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompositorID,Nome")] Compositor compositor)
        {
            if (ModelState.IsValid)
            {
                _context.Add(compositor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(compositor);
        }

        // GET: Compositores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var compositor = await _context.Compositor.FindAsync(id);
            if (compositor == null)
            {
                return NotFound();
            }
            return View(compositor);
        }

        // POST: Compositores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompositorID,Nome")] Compositor compositor)
        {
            if (id != compositor.CompositorID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(compositor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompositorExists(compositor.CompositorID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(compositor);
        }

        // GET: Compositores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var compositor = await _context.Compositor
                .FirstOrDefaultAsync(m => m.CompositorID == id);
            if (compositor == null)
            {
                return NotFound();
            }

            return View(compositor);
        }

        // POST: Compositores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var compositor = await _context.Compositor.FindAsync(id);
            _context.Compositor.Remove(compositor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompositorExists(int id)
        {
            return _context.Compositor.Any(e => e.CompositorID == id);
        }
    }
}
