﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("FicheiroVersoes")]
    public class FicheiroVersao
    {
        [Key]
        public int FicheiroVersaoID { get; set; }
        public int FicheiroID { get; set; }
        public int VersaoCanticoID { get; set; }
        public virtual VersaoCantico VersaoCantico { get; set; }
        public virtual Ficheiro Ficheiro { get; set; }
    }
}