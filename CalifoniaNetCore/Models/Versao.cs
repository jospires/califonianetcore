﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CalifoniaNetCore.Models
{
    [Table("Versoes")]
    public class Versao
    {
        [Key]
        public int VersaoID { get; set; }

        [Display(Name = "Versão:")]
        public string Nome { get; set; }
        public ICollection<VersaoCantico> VersoesCanticos { get; set; }
    }
}
