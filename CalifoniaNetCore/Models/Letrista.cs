﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("Letristas")]
    public class Letrista
    {
        [Key]
        public int LetristaID { get; set; }

        [Display(Name = "Nome:")]
        public string Nome { get; set; }
        public ICollection<Cantico> Canticos { get; set; }
    }
}