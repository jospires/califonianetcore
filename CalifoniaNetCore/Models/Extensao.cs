﻿using CalifoniaNetCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("Extensoes")]
    public class Extensao
    {
        [Key]
        public int ExtensaoID { get; set; }

        [Display(Name = "Extensão:")]
        public string Nome { get; set; }
        public virtual ICollection<Ficheiro> Ficheiros { get; set; }
    }
}