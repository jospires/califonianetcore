﻿using CalifoniaNetCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("Arranjadores")]
    public class Arranjador
    {
        [Key]
        public int ArranjadorID { get; set; }

        [Display(Name = "Nome:")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter {2} e {1} caracteres", MinimumLength = 3)]
        public string Nome { get; set; }
        //public ICollection<Versao> Versao { get; set; }
        public virtual ICollection<VersaoCantico> VersaoCantico { get; set; }
    }
}