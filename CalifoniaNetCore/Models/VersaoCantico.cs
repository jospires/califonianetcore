﻿using Califonia.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("VersoesCanticos")]
    public class VersaoCantico
    {
        [Key]
        public int VersaoCanticoID { get; set; }
        public int VersaoID { get; set; }
        public int CanticoID { get; set; }
        public int ArranjadorID { get; set; }

        [Display(Name = "Data do Arranjo:")]
        [DataType(DataType.Date)]
        public DateTime DataVersao { get; set; }
        public virtual Versao Versao { get; set; }
        public virtual Cantico Cantico { get; set; }
        public virtual Arranjador Arranjador { get; set; }
        public ICollection<FicheiroVersao> FicheiroVersoes { get; set; }
    }
}