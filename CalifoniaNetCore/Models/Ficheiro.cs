﻿using Califonia.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("Ficheiros")]
    public class Ficheiro
    {
        [Key]
        public int FicheiroID { get; set; }

        [Display(Name = "Ficheiro:")]
        public string Nome { get; set; }

        [Display(Name = "Endereço:")]
        public string Url { get; set; }

        [Display(Name = "Extensao:")]
        public int ExtensaoID { get; set; }
        public virtual Extensao Extensao { get; set; }
        public ICollection<FicheiroVersao> FicheiroVersoes { get; set; }
    }
}