﻿using Califonia.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("PublicacoesCanticos")]
    public class PublicacaoCantico
    {
        [Key]
        public int PublicacaoCanticoID { get; set; }
        public int PublicacaoID { get; set; }
        public int CanticoID { get; set; }

        [Display(Name = "Número de página:")]
        [Range(0, int.MaxValue, ErrorMessage = "Insira um valor maior do que {0}")]
        public int NrPagina { get; set; }
        public virtual Cantico Cantico { get; set; }
        public virtual Publicacao Publicacao { get; set; }
    }
}