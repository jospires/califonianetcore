﻿using CalifoniaNetCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("Compositores")]
    public class Compositor
    {
        [Key]
        public int CompositorID { get; set; }

        [Display(Name = "Nome:")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter {2} e {1} caracteres", MinimumLength = 3)]
        public string Nome { get; set; }
        public ICollection<Cantico> Canticos { get; set; }
    }
}