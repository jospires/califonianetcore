﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("Publicacoes")]
    public class Publicacao
    {
        [Key]
        public int PublicacaoID { get; set; }

        [Display(Name = "Nome:")]
        [StringLength(30, ErrorMessage = "O campo {0} deverá conter {2} e {1} caracteres", MinimumLength = 3)]
        public string Nome { get; set; }

        [Display(Name = "Data da Publicação:")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd-mm-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DataPublicacao { get; set; }

        [Display(Name = "Edição:")]
        public string Edicao { get; set; }

        //public ICollection<PublicacaoCantico> PublicacaoCantico { get; set; }
    }
}