﻿using Califonia.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("Canticos")]
    public class Cantico
    {
        [Key]
        public int CanticoID { get; set; }

        [Display(Name = "Título:")]
        [Required(ErrorMessage = "Este campo é ")]
        [StringLength(100, ErrorMessage = "O campo {0} deverá conter {2} e {1} caracteres", MinimumLength = 3)]
        public string Titulo { get; set; }

        [Display(Name = "Data da Composição:")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd-mm-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DataComposicao { get; set; }

        [Display(Name = "Data de Submissão:")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:dd-mm-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DataSubmissao { get; set; }

        [Range(1, double.MaxValue, ErrorMessage = ("Tem que seleccionar um {0}"))]
        [Display(Name = "Letrista:")]
        public int LetristaID { get; set; }

        public virtual Letrista Letrista { get; set; }

        //[Required(ErrorMessage = "O {0} é obrigatório")]
        //[Range(1, double.MaxValue, ErrorMessage = ("Tem que seleccionar uma {0}"))]
        //[Display(Name = "Letra:")]
        //public int LetraID { get; set; }

        //public virtual Letra Letra { get; set; }

        [Required(ErrorMessage = "O {0} é obrigatório")]
        [Range(1, double.MaxValue, ErrorMessage = ("Tem que seleccionar um {0}"))]
        [Display(Name = "Compositor:")]
        public int CompositorID { get; set; }
        public virtual Compositor Compositor { get; set; }
        public string Notas { get; set; }
        public ICollection<PublicacaoCantico> PublicacoesCanticos { get; set; }
        public ICollection<TagCantico> TagsCanticos { get; set; }
        public ICollection<VersaoCantico> VersoesCanticos { get; set; }
        public ICollection<LetraCantico> LetrasCanticos { get; set; }

    }
}