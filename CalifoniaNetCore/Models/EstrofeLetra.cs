﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("EstrofesLetras")]
    public class EstrofeLetra
    {
        [Key]
        public int EstrofeLetraID { get; set; }
        [Display(Name = "Descrição:")]
        public string Descricao { get; set; }
        [Display(Name = "Estrofe:")]
        public string TextoEstrofe { get; set; }
        public ICollection<Letra> Letras { get; set; }
    }
}