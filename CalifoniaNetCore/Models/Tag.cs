﻿using CalifoniaNetCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("Tags")]
    public class Tag
    {
        [Key]
        public int TagID { get; set; }

        [Display(Name = "Filtro:")]
        public string Nome { get; set; }
        public ICollection<TagCantico> TagsCanticos { get; set; }
    }
}