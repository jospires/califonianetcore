﻿using Califonia.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CalifoniaNetCore.Models
{
    [Table("TagsCanticos")]
    public class TagCantico
    {
        [Key]
        public int TagCanticoID { get; set; }
        public int CanticoID { get; set; }
        public int TagID { get; set; }
        public virtual Cantico Cantico { get; set; }
        public virtual Tag Tag { get; set; }
    }
}