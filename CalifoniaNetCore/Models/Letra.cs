﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Califonia.Models
{
    [Table("Letras")]
    public class Letra
    {
        [Key]
        public int LetraID { get; set; }

        [Display(Name = "Refrao:")]
        public int RefraoLetraID { get; set; }

        [Display(Name = "Estrofe:")]
        public int EstrofeLetraID { get; set; }
        public virtual EstrofeLetra EstrofeLetra { get; set; }
        public virtual RefraoLetra RefraoLetra { get; set; }
        public ICollection<LetraCantico> LetraCantico { get; set; }
    }
}