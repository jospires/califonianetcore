﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("RefraesLetras")]
    public class RefraoLetra
    {
        [Key]
        public int RefraoLetraID { get; set; }
        [Display(Name = "Refrao:")]
        public string TextoRefrao { get; set; }
        public ICollection<Letra> Letras { get; set; }
    }
}