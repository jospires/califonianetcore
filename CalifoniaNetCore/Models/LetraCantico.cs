﻿using CalifoniaNetCore.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Califonia.Models
{
    [Table("LetrasCanticos")]
    public class LetraCantico
    {
        [Key]
        public int LetraCanticoID { get; set; }
        public int LetraID { get; set; }
        public int CanticoID { get; set; }
        public virtual Cantico Cantico { get; set; }
        public virtual Letra Letra { get; set; }
    }
}