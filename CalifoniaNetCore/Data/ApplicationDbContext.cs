﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Califonia.Models;
using CalifoniaNetCore.Models;

namespace CalifoniaNetCore.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Arranjador> Arranjador { get; set; }
        public DbSet<Compositor> Compositor { get; set; }
        public DbSet<Extensao> Extensao { get; set; }
        public DbSet<Letra> Letra { get; set; }
        public DbSet<Publicacao> Publicacao { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<CalifoniaNetCore.Models.Letrista> Letrista { get; set; }
        public DbSet<CalifoniaNetCore.Models.Ficheiro> Ficheiro { get; set; }
        public DbSet<Califonia.Models.RefraoLetra> RefraoLetra { get; set; }
        public DbSet<Califonia.Models.EstrofeLetra> EstrofeLetra { get; set; }
        public DbSet<CalifoniaNetCore.Models.Cantico> Cantico { get; set; }
        
    }
}
